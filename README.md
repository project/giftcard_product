# Giftcard product

This module provides a product type and order item type for giftcards.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/giftcard_product).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/giftcard_product).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires:

- [Drupal Commerce](https://www.drupal.org/project/commerce)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

No configuration is needed.


## Maintainers

- Eirik Morland - [eiriksm](https://www.drupal.org/u/eiriksm)
- Mykhailo Gurei - [ozin](https://www.drupal.org/u/ozin)
