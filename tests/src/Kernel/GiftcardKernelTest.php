<?php

namespace Drupal\Tests\giftcard_product\Kernel;

use Drupal\commerce_order\Entity\OrderItemType;
use Drupal\commerce_product\Entity\ProductType;
use Drupal\commerce_product\Entity\ProductVariationType;
use Drupal\Tests\commerce\Kernel\CommerceKernelTestBase;

/**
 * Test giftcard low level things.
 *
 * @group giftcard_product
 */
class GiftcardKernelTest extends CommerceKernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'giftcard_product',
    'commerce_product',
    'commerce_order',
    'profile',
    'entity_reference_revisions',
    'state_machine',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() : void {
    parent::setUp();
    $this->installEntitySchema('commerce_product');
    $this->installEntitySchema('commerce_product_variation');
    $this->installEntitySchema('commerce_order_item');
    $this->installConfig('giftcard_product');
  }

  /**
   * Test that the types we created are there.
   */
  public function testProductTypeCreated() {
    $type = OrderItemType::load('giftcard');
    $this->assertEquals($type->id(), 'giftcard');
    $type = ProductType::load('giftcard');
    $this->assertEquals($type->id(), 'giftcard');
    $type = ProductVariationType::load('giftcard');
    $this->assertEquals($type->id(), 'giftcard');
  }

}
