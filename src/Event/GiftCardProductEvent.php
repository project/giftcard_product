<?php

namespace Drupal\giftcard_product\Event;

use Drupal\commerce_order\Entity\OrderInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class for the event.
 */
class GiftCardProductEvent extends Event {

  /**
   * Name of the event fired after order with giftcard was placed.
   *
   * @Event
   */
  const GIFTCARD_PLACE = 'giftcard_product.place';

  /**
   * Name of the event fired after order with giftcard was paid.
   *
   * @Event
   */
  const GIFTCARD_PAID = 'giftcard_product.paid';

  /**
   * The order entity.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * The giftcard variation.
   *
   * @var \Drupal\commerce_product\Entity\ProductVariationInterface[]
   */
  protected $giftcards;

  /**
   * GiftCardProductEvent constructor.
   */
  public function __construct(OrderInterface $order, array $giftcards) {
    $this->order = $order;
    $this->giftcards = $giftcards;
  }

  /**
   * Get giftcard.
   *
   * @return \Drupal\commerce_product\Entity\ProductVariationInterface[]
   *   Giftcards.
   */
  public function getGiftcards() {
    return $this->giftcards;
  }

  /**
   * Get order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   Order.
   */
  public function getOrder() {
    return $this->order;
  }

}
