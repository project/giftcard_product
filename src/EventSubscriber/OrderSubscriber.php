<?php

namespace Drupal\giftcard_product\EventSubscriber;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_order\Event\OrderEvents;
use Drupal\giftcard_product\Event\GiftCardProductEvent;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * The subscriber for doing work.
 */
class OrderSubscriber implements EventSubscriberInterface {

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Giftcard order item type.
   *
   * @var string
   */
  protected $giftcardOrderType;

  /**
   * OrderSubscriber constructor.
   */
  public function __construct(EventDispatcherInterface $event_dispatcher) {
    $this->eventDispatcher = $event_dispatcher;
    $this->giftcardOrderType = 'giftcard';
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'commerce_order.place.post_transition' => ['onPlace'],
      OrderEvents::ORDER_PAID => ['onPaidOrder'],
    ];
  }

  /**
   * Check if we have giftcard order item and trigger our event.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   */
  public function onPlace(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    if ($giftcards = $this->getOrderGiftcards($order)) {
      $event = new GiftCardProductEvent($order, $giftcards);
      $this->eventDispatcher->dispatch(GiftCardProductEvent::GIFTCARD_PLACE, $event);
    }
  }

  /**
   * Dispatch event when order with giftcard is paid.
   *
   * @param \Drupal\commerce_order\Event\OrderEvent $event
   *   Order event.
   */
  public function onPaidOrder(OrderEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getOrder();
    if ($giftcards = $this->getOrderGiftcards($order)) {
      $event = new GiftCardProductEvent($order, $giftcards);
      $this->eventDispatcher->dispatch(GiftCardProductEvent::GIFTCARD_PAID, $event);
    }
  }

  /**
   * Get order giftcards.
   */
  public function getOrderGiftcards(OrderInterface $order) {
    $giftcards = [];
    /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
    foreach ($order->getItems() as $order_item) {
      if ($order_item->bundle() === $this->giftcardOrderType) {
        /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $product_variation */
        $giftcards[] = $order_item->getPurchasedEntity();
      }
    }

    return $giftcards;
  }

}
